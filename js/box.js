/*DANIMON NGABA Nedjim */
/*Simplon - Promo 6 */

console.log("File: box.js open");

/*------------------Activation des box------------- */
Array.prototype.forEach.call(block, function(box) {

    function song() {

        if (box.classList.contains("yellow")) {
            playPause(audio1);
        }
        if (box.classList.contains("orange")) {
            playPause(audio2);
        }
        if (box.classList.contains("pink")) {
            playPause(audio3);
        }
        if (box.classList.contains("violet")) {
            playPause(audio4);
        }
        if (box.classList.contains("blue")) {
            playPause(audio5);
        }
        if (box.classList.contains("green")) {
            playPause(audio6);
        }
    }

    box.addEventListener("click", song);
});

/*
Remarque: 
J'ai d'abord essayé d'utiliser un menu (switch) mais comme la valeur de retour 
de classList.contains("") est un booléen, je fini par utiliser le if
 */