/*DANIMON NGABA Nedjim */
/*Simplon - Promo 6 */

console.log("File: controllers.js open");

/*------------Déclaration des variables globales------------*/
var audio1 = document.getElementById("audio1");
var audio2 = document.getElementById("audio2");
var audio3 = document.getElementById("audio3");
var audio4 = document.getElementById("audio4");
var audio5 = document.getElementById("audio5");
var audio6 = document.getElementById("audio6");

var block = document.getElementsByClassName("box");

/*------------------Fonction Play / Pause--------------------*/
function playPause(audio) {

    if (audio.paused) {
        audio.play();
        audio.loop = true;

    } else {
        audio.pause();
    }
}